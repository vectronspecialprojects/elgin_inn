import React, {useState, useCallback, useEffect, useMemo} from 'react'
import {View, FlatList, StyleSheet, Text, RefreshControl, Linking} from 'react-native'
import {useDispatch, useSelector} from 'react-redux'
import * as infoServicesActions from '../store/actions/infoServices'
import AddressCard from './../components/AddressCard'
import SubHeaderBar from './../components/SubHeaderBar'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import MapDetail from './../components/MapDetail'
import VenueTags from './../components/VenueTags'
import RouteKey from '../navigation/RouteKey'
import analytics from '@react-native-firebase/analytics'
import {openMaps} from '../utilities/utils'
import Geolocation from 'react-native-geolocation-service'
import Geocoder from '@timwangdev/react-native-geocoder'
import HeaderLeftButton from '../components/HeaderLeftButton'
import {isMultiVenue} from '../constants/env'
import {isIOS} from '../Themes/Metrics'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'

const LocationsScreen = (props) => {
  const venues = useSelector((state) => state.infoServices.venues)
  const venueTags = useSelector((state) => state.infoServices.venueTags)
  const venueTagsSetting = useSelector((state) => state.infoServices.venueTagsSetting)
  const title = props.route.params.params?.page_name
  const dispatch = useDispatch()
  const [mapLocation, setMapLocation] = useState()
  const [isRefreshing, setIsRefreshing] = useState(false)
  const [selectedVenue, setSelectedVenue] = useState(null)
  const [selectedTag, setSelectedTag] = useState(1)
  const [isOpen, setIsOpen] = useState(false)

  const verifyPermissions = async () => {
    const result = await Geolocation.requestAuthorization('whenInUse')
    if (result !== 'granted') {
      Alert.alert(localize('location.titlePer'), localize('location.titleMess'), [{text: localize('okay')}])
      return false
    }
    return true
  }

  const venueHandler = async (venue) => {
    if (selectedVenue === venue.id) {
      setIsOpen(false)
      setSelectedVenue(null)
    } else {
      let hasPermission = true
      if (isIOS()) {
        hasPermission = await verifyPermissions()
      }
      if (!hasPermission) {
        return
      } else {
        if (venue.address) {
          try {
            const venuePosition = await Geocoder.geocodeAddress(venue.address, {maxResult: 1, locale: 'us'})
            setMapLocation({
              lat: venuePosition[0]?.position?.lat,
              lng: venuePosition[0]?.position?.lng,
              name: venue.name,
              address: venue.address,
            })
          } catch (err) {
            Alert.alert(localize('location.titleFetch'), localize('location.plsTryAgain'), [{text: 'Okay'}])
          }
        }
        setIsOpen(true)
        setSelectedVenue(venue.id)
      }
    }
  }

  const linkHandler = (uri) => {
    Linking.openURL(uri)
  }

  const loadContent = useCallback(async () => {
    setIsRefreshing(true)
    try {
      await dispatch(infoServicesActions.fetchVenues())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
    }
  }, [dispatch])

  useEffect(() => {
    async function firebase() {
      await analytics().logScreenView({screen_name: 'Find us'})
    }
    firebase()
  }, [])

  useEffect(() => {
    if (!isMultiVenue) venueHandler(venues[0])
  }, [venues, isMultiVenue])

  const callMapHandler = () => {
    props.navigation.navigate(RouteKey.MapScreen, {location: mapLocation})
  }

  const direction = () => {
    const location = {
      latitude: mapLocation.lat,
      longitude: mapLocation.lng,
      provider: 'google',
      start: 'My Location',
      end: `${mapLocation.lat},${mapLocation.lng}`,
    }
    openMaps(location)
  }

  const showData = useMemo(() => {
    let venuesList = []
    if (!isMultiVenue) return venues.filter((venue) => venue.name === 'ALL VENUES')
    if (venueTagsSetting.isTagEnabled === 'false') return venues
    if (selectedTag === 1) return venues
    venues.forEach((venue) => {
      if (venue.venue_pivot_tags.filter((tag) => tag.venue_tag_id === selectedTag).length > 0)
        venuesList = venuesList.concat(venue)
    })
    return venuesList
  }, [venues, venueTags, selectedTag, venueTagsSetting, isMultiVenue])

  const renderItem = (itemData) => {
    const venue = itemData.item
    return (
      <View>
        {isMultiVenue && (
          <AddressCard
            imgSource={venue?.image}
            venueName={venue?.name}
            venueAddress={venue?.address}
            onPress={venueHandler.bind(this, venue)}
          />
        )}
        {(!isMultiVenue || (selectedVenue === venue?.id && isOpen)) && (
          <MapDetail
            onMapPress={callMapHandler}
            location={mapLocation}
            address={venue?.address}
            telp={venue?.telp}
            onTelpPress={() => linkHandler(`tel: ${venue?.telp || 0}`)}
            email={venue?.email}
            onEmailPress={linkHandler.bind(this, 'mailto:' + venue?.email)}
            onDirectionsPress={direction}
            openHours={JSON.parse(venue?.open_and_close_hours)}
            isHideOpenHours={venue?.hide_opening_hours_info}
            DeliveryHours={JSON.parse(venue?.pickup_and_delivery_hours)}
            isHideDeliveryHours={venue?.hide_delivery_info}
            socialLinks={JSON.parse(venue?.social_links)}
            menuLinks={JSON.parse(venue?.menu_links)}
          />
        )}
      </View>
    )
  }

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={title} />
      {venueTagsSetting?.isTagEnabled === 'true' && isMultiVenue && (
        <VenueTags
          data={venueTags.sort((a, b) => a?.display_order - b?.display_order)}
          selectedTag={selectedTag}
          onPress={(id) => setSelectedTag(id)}
        />
      )}
      <View style={{flex: 1}}>
        <FlatList
          data={showData}
          renderItem={renderItem}
          keyExtractor={(item) => item.id.toString()}
          ListEmptyComponent={
            <Text style={Styles.flatlistNoItems}>No venues found, please check again later.</Text>
          }
          refreshControl={
            <RefreshControl
              refreshing={isRefreshing}
              onRefresh={loadContent}
              tintColor={Colors.defaultRefreshSpinner}
              titleColor={Colors.defaultRefreshSpinner}
              title={localize('pullToRefresh')}
            />
          }
        />
      </View>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({
  MapPreview: {
    width: '100%',
    height: 200,
  },
  directions: {
    position: 'absolute',
    bottom: 0,
    marginVertical: 10,
    paddingHorizontal: 8,
  },
})

export default LocationsScreen
