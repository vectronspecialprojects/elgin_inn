import React from 'react'
import {View, Text} from 'react-native'
import Styles from '../../Themes/Styles'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import SubHeaderBar from '../../components/SubHeaderBar'

const ShopScreen = (props) => {
  const title = props.route.params.params?.page_name
  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={title} filterBtn={false} />
      <Text>This is ShopScreen</Text>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default ShopScreen
