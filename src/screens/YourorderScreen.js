import React, {useMemo, useEffect, useState} from 'react'
import {View, ActivityIndicator} from 'react-native'
import {WebView} from 'react-native-webview'
import {useDispatch, useSelector} from 'react-redux'
import Colors from '../Themes/Colors'
import Styles from '../Themes/Styles'
import HeaderLeftButton from '../components/HeaderLeftButton'
import Alert from '../components/Alert'
import {venueWebsite} from '../constants/env'
import {localize} from '../locale/I18nConfig'
import {checkTokenValid} from '../store/actions/authServices'

const YouorderScreen = (props) => {
  const [idToken, setValidToken] = useState('')
  const dispatch = useDispatch()
  const dynamicUri = useSelector(
    (state) => state.infoServices.profile.member.current_preferred_venue_full?.your_order_link,
  )

  const staticUri = props.route.params.params?.special_link
  const qrcodeUri = props.route.params.params?.qrcodeUri
  useEffect(() => {
    dispatch(
      checkTokenValid((value) => {
        setValidToken(value?.id_token)
      }),
    )
  }, [])

  const uri = useMemo(() => {
    if (qrcodeUri) return `${qrcodeUri}&id_token=${idToken}`
    if (staticUri) return `${staticUri}&id_token=${idToken}`
    else if (!staticUri && dynamicUri) return `${dynamicUri}&id_token=${idToken}`
    return venueWebsite
  }, [dynamicUri, staticUri, qrcodeUri, idToken])

  useEffect(() => {
    if (!uri) {
      Alert.alert(localize('yourOrder.notAvailable'), localize('yourOrder.message'), [
        {text: localize('okay')},
      ])
    }
  }, [uri])

  return (
    <View style={Styles.screen}>
      {!!idToken && (
        <View style={{flex: 1}}>
          <WebView
            startInLoadingState={true}
            source={{uri: uri}}
            renderLoading={() => (
              <View
                style={{
                  backgroundColor: Colors.defaultBackground,
                  position: 'absolute',
                  top: '50%',
                  left: '50%',
                  right: '50%',
                }}>
                <ActivityIndicator size="large" />
              </View>
            )}
          />
        </View>
      )}
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default YouorderScreen
