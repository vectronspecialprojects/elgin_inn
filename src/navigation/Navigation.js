import React from 'react'
import {StyleSheet} from 'react-native'
import {useSelector, useDispatch} from 'react-redux'
import {createDrawerNavigator, DrawerItemList} from '@react-navigation/drawer'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import {componentMatch} from './StackNavigation'
import {DrawerCtnOptions, tabOptions} from './DefaultOptions'
import * as authServicesActions from '../store/actions/authServices'
import RouteKey from './RouteKey'
import FontAwesome5Pro from 'react-native-vector-icons/FontAwesome5Pro'
import {isIOS} from '../Themes/Metrics'
import {DrawerLogo} from './drawerComponents/DrawerLogo'
import LogOut from './drawerComponents/LogOut'

//tab
const TabStackNavigator = createBottomTabNavigator()
export const TabNavigator = (props) => {
  const tabMenus = useSelector((state) => state.app.tabMenus)
  return (
    <TabStackNavigator.Navigator backBehavior="initialRoute" screenOptions={tabOptions}>
      {tabMenus.map((tabMenu) => {
        if (tabMenu.icon === '' || tabMenu.state === '') return null
        return (
          <TabStackNavigator.Screen
            key={tabMenu.id}
            name={tabMenu.id === 1 ? RouteKey.HomeNavigator : RouteKey.tabMenu + tabMenu.id}
            component={componentMatch(tabMenu.state)}
            initialParams={{params: tabMenu}}
            options={{
              headerShown: false,
              tabBarLabel: tabMenu.page_name,
              unmountOnBlur: true,
              tabBarIcon: (props) => (
                <FontAwesome5Pro
                  name={tabMenu.icon}
                  style={{marginTop: isIOS() ? 10 : 0}}
                  size={22}
                  color={props.color}
                />
              ),
            }}
          />
        )
      })}
    </TabStackNavigator.Navigator>
  )
}

//drawer after signin
const DrawerStackAfterAuth = createDrawerNavigator()
export const DrawerAfterAuth = () => {
  const dispatch = useDispatch()
  const drawerMenus = useSelector((state) => state.app.drawerMenus)
  return (
    <DrawerStackAfterAuth.Navigator
      drawerContent={(props) => (
        <DrawerLogo>
          <DrawerItemList {...props} />
          <LogOut
            onPress={() => {
              props.navigation.toggleDrawer()
              dispatch(authServicesActions.logout())
            }}
          />
        </DrawerLogo>
      )}
      screenOptions={DrawerCtnOptions}>
      <DrawerStackAfterAuth.Screen
        name={RouteKey.Home}
        component={TabNavigator}
        options={{
          headerShown: false,
          drawerLabel: 'Home',
          drawerIcon: (props) => (
            <FontAwesome5Pro name="home" style={{width: 30}} size={22} color={props.color} />
          ),
        }}
      />

      {drawerMenus.map((drawerMenu) => {
        if (drawerMenu.icon === '') return null
        return (
          <DrawerStackAfterAuth.Screen
            key={drawerMenu.id}
            name={RouteKey.sideMenu + drawerMenu.id}
            component={componentMatch(drawerMenu.state)}
            initialParams={{params: drawerMenu}}
            options={{
              unmountOnBlur: true,
              headerShown: false,
              drawerLabel: drawerMenu.page_name,
              drawerIcon: (props) => (
                <FontAwesome5Pro name={drawerMenu.icon} size={22} style={{width: 30}} color={props.color} />
              ),
            }}
          />
        )
      })}
    </DrawerStackAfterAuth.Navigator>
  )
}

//drawer before signin
const DrawerStackBeforeAuth = createDrawerNavigator()
export const DrawerBeforeAuth = () => {
  const drawerFrontMenus = useSelector((state) => state.app.drawerFrontMenus)
  return (
    <DrawerStackBeforeAuth.Navigator
      drawerContent={(props) => (
        <DrawerLogo>
          <DrawerItemList {...props} />
        </DrawerLogo>
      )}
      screenOptions={DrawerCtnOptions}>
      <DrawerStackBeforeAuth.Screen
        name={RouteKey.FrontpageNavigator}
        component={componentMatch(RouteKey.FrontpageNavigator)}
        options={{
          headerShown: false,
          drawerLabel: 'Home',
          drawerIcon: (props) => (
            <FontAwesome5Pro name="home" size={22} style={{width: 30}} color={props.color} />
          ),
        }}
      />

      {drawerFrontMenus.map((drawerFrontMenu) => {
        if (drawerFrontMenu.icon === '') return null
        return (
          <DrawerStackBeforeAuth.Screen
            key={drawerFrontMenu.id}
            name={RouteKey.sideFrontMenu + drawerFrontMenu.id}
            component={componentMatch(drawerFrontMenu.state)}
            initialParams={{params: drawerFrontMenu}}
            options={{
              unmountOnBlur: true,
              headerShown: false,
              drawerLabel: drawerFrontMenu.page_name,
              drawerIcon: (props) => (
                <FontAwesome5Pro
                  name={drawerFrontMenu.icon}
                  size={22}
                  style={{width: 30}}
                  color={props.color}
                />
              ),
            }}
          />
        )
      })}
    </DrawerStackBeforeAuth.Navigator>
  )
}
