import React from 'react'
import {StyleSheet, Platform, View, Text, TouchableOpacity, Animated, SafeAreaView} from 'react-native'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Colors from '../Themes/Colors'
import {useNavigation} from '@react-navigation/native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Fonts from '../Themes/Fonts'
import Ionicons from 'react-native-vector-icons/Ionicons'

function Header({
  title,
  containerStyle = {},
  hasBackButton = true,
  leftComponent,
  centerComponent,
  rightComponent,
  titleColor = Colors.white,
  titleStyle,
  backButtonPress,
  rightStyle,
  transparent = false,
  hasLeft = true,
}) {
  const navigation = useNavigation()
  return (
    <SafeAreaView style={{backgroundColor: transparent ? 'transparent' : Colors.dark}}>
      <View style={[styles.headerStyle, containerStyle]}>
        {hasLeft && (
          <View style={{flex: 1, justifyContent: 'center'}}>
            {hasBackButton && (
              <TouchableOpacity
                hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                onPress={() => {
                  if (backButtonPress) {
                    backButtonPress()
                  } else {
                    navigation.pop()
                  }
                }}>
                <Ionicons name={'md-close'} color={'#CACFD3'} size={30} />
              </TouchableOpacity>
            )}
            {!!leftComponent && leftComponent}
          </View>
        )}

        <Animated.View
          style={[
            {
              flex: 4,
              justifyContent: 'center',
            },
            titleStyle,
          ]}>
          {!!title && (
            <Text
              style={[
                styles.textTitle,
                {color: titleColor},
                hasLeft && {textAlign: 'center', fontSize: responsiveFont(16)},
              ]}
              numberOfLines={1}>
              {title}
            </Text>
          )}
          {!!centerComponent && <View style={{flex: 1}}>{centerComponent}</View>}
        </Animated.View>

        <View style={[{flex: 1, justifyContent: 'center'}, rightStyle]}>
          {!!rightComponent && rightComponent}
        </View>
      </View>
    </SafeAreaView>
  )
}

export default Header

const styles = StyleSheet.create({
  textTitle: {
    fontSize: responsiveFont(24)
  },
  headerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: responsiveWidth(15),
    height: responsiveHeight(50),
  },
})
