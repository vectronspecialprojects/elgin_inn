import React from 'react'
import {View, StyleSheet, Image, Text} from 'react-native'
import Colors from '../Themes/Colors'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Fonts from '../Themes/Fonts'
import {TouchableCmp} from './UtilityFunctions'
import FastImage from 'react-native-fast-image'
function FavoriteItem({data, onPress}) {
  return (
    <TouchableCmp onPress={onPress}>
      <View style={styles.container}>
        <FastImage source={{uri: data?.image_square}} style={styles.image} />
        <Text style={styles.name}>{data?.name}</Text>
      </View>
    </TouchableCmp>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.first,
    padding: responsiveWidth(5),
    borderRadius: responsiveWidth(10),
    marginRight: responsiveWidth(5),
    marginBottom: responsiveHeight(12),
    width: responsiveWidth(104),
  },
  image: {
    width: responsiveWidth(94),
    height: responsiveWidth(94),
    borderRadius: responsiveWidth(10),
  },
  name: {
    fontSize: responsiveFont(10),
    color: Colors.favorite.description,
    fontFamily: Fonts.openSans,
    textAlign: 'center',
    marginTop: responsiveHeight(10),
  },
})

export default FavoriteItem
