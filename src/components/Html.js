import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import HTML from 'react-native-render-html'
import {deviceWidth, responsiveFont} from '../Themes/Metrics'

const Html = (props) => {
  // console.log('HTML', props.color, props.html)
  return (
    <HTML
      source={{
        html: props.html
          .replace(/&lt;/g, '<')
          .replace(/&gt;/g, '>')
          .replace(/&nbsp;/g, ' '),
      }}
      tagsStyles={{
        div: {textAlign: props.textAlign, color: props.color},
        li: {color: props.color},
        p: {textAlign: props.textAlign, color: props.color},
        ul: {marginTop: 20, color: props.color},
        del: {textDecorationLine: 'line-through', textDecorationStyle: 'solid'},
        ol: {color: props.color},
        blockquote: {color: props.color},
      }}
      baseStyle={{color: props.color, textAlign: props.textAlign}}
      renderers={{
        blockquote: ({TDefaultRenderer, ...customProps}) => {
          return (
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={{color: props.color}}>"</Text>
              <TDefaultRenderer {...customProps} style={{paddingHorizontal: 2}} />
              <Text style={{color: props.color}}>"</Text>
            </View>
          )
        },
      }}
      renderersProps={{a: {onPress: props.onLinkPress}}}
      contentWidth={deviceWidth()}
    />
  )
}

const styles = StyleSheet.create({
  bullet: {
    marginRight: 5,
    width: 6,
    height: 6,
    marginTop: 5,
    borderRadius: 3,
  },
  textStyle: {
    marginRight: 5,
    fontSize: responsiveFont(14),
  },
})

export default Html
