import {Platform} from 'react-native';

const buildEvn = {
  production: 'production',
  staging: 'staging',
}

const vars = {
  googleApiKey: 'AIzaSyAl60Q1FOsJeZ4RGaF5KCp_Kcf9sGM6p3A',
  oneSignalApiKey: '24e2bfdd-7d98-4b39-88d1-772e2bcc547b',
  buildEvn: buildEvn.production,
}

export const codePushKey = Platform.select({
  ios: {
    staging: 'rw3FqDgTDJzfehS4h-gnPx8-PIH3ZkT6KVo1o',
    production: 'LnfsGOh6ZU2GHxx61REirudU6RmH3CcH9buQB',
  },
  android: {
    staging: 'gx-rEwwxSmLueLyY9taZ_YGSDaaSVA5QzTwxf',
    production: 'a0QXGVAObgoCn3R49cZhTArNarZe725KoXAsH',
  },
})

export const venueName = 'Elgin Inn'
export const venueWebsite = 'https://www.elgininn.com.au'

//startup page setup
export const isShowLogo = true

//sign up page setup
export const isMultiVenue = false
export const accountMatch = false

export const isGaming = false //need to be true to allow gamingOdyssey or gamingIGT
export const gamingOdyssey = false
export const gamingIGT = false

export const showTierSignup = false //need to be true to allow isCustomField and showMultipleExtended
export const isCustomField = true //need to be true to allow showMultipleExtended
export const showMultipleExtended = true

//home page setup
export const isShowStarBar = false
export const showTierBelowName = false
export const showPointsBelowName = true
export const isShowPreferredVenue = false
export const showTier = true

export const isGroupFilter = false // must be multi venue and has venue tags
export default vars
